package Fauteuil
  package Batterie
    model   Model
      Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage1(V = 12) annotation(
        Placement(visible = true, transformation(origin = {0, 28}, extent = {{-10, 10}, {10, -10}}, rotation = 0)));
      output Real E;
      output Real Q;
      Modelica.Electrical.Analog.Basic.Capacitor capacitor(C = 200) annotation(
        Placement(visible = true, transformation(origin = {-32, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor resistor(R = 60) annotation(
        Placement(visible = true, transformation(origin = {-32, -76}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Ideal.ControlledIdealTwoWaySwitch switch(level = 11.9) annotation(
        Placement(visible = true, transformation(origin = {34, -20}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
      Modelica.Electrical.Analog.Basic.Resistor R(R = 10) annotation(
        Placement(visible = true, transformation(origin = {-42, 12}, extent = {{-10, -10}, {10, 10}}, rotation = -90)));
      Modelica.Electrical.Analog.Basic.Ground ground1 annotation(
        Placement(visible = true, transformation(origin = {66, 4}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
    equation
      E = 0.5 * capacitor.C * capacitor.v * capacitor.v;
      Q = capacitor.C * capacitor.v;
  connect(capacitor.p, resistor.p) annotation(
        Line(points = {{-42, -20}, {-42, -76}}, color = {0, 0, 255}));
  connect(capacitor.n, switch.p) annotation(
        Line(points = {{-22, -20}, {24, -20}}, color = {0, 0, 255}));
  connect(switch.n2, resistor.n) annotation(
        Line(points = {{44, -20}, {44, -76}, {-22, -76}}, color = {0, 0, 255}));
  connect(switch.n1, constantVoltage1.n) annotation(
        Line(points = {{44, -16}, {44, 28}, {10, 28}}, color = {0, 0, 255}));
  connect(ground1.p, constantVoltage1.n) annotation(
        Line(points = {{66, 14}, {66, 28}, {10, 28}}, color = {0, 0, 255}));
  connect(constantVoltage1.p, R.p) annotation(
        Line(points = {{-10, 28}, {-42, 28}, {-42, 22}}, color = {0, 0, 255}));
  connect(R.n, capacitor.p) annotation(
        Line(points = {{-42, 2}, {-42, -20}}, color = {0, 0, 255}));
  connect(switch.control, capacitor.p) annotation(
        Line(points = {{34, -10}, {-42, -10}, {-42, -20}}, color = {0, 0, 255}));
    end Model;
  end Batterie;
  annotation(
    uses(Modelica(version = "4.0.0")));
end Fauteuil;
